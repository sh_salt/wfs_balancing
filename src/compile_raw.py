'''
Source data and compile into suitable input for further processing

For a given night:
1. Download tracker data
2. Download positioner data
3. Download guidance data
4. Filter out junk points
  * SN too low
  * No change
  * Out of focus
5. Merge data
6. Filter out NaNs
7. Save as pickle
'''

import argparse
# import requests
from datetime import datetime, timedelta

RAW_DIR = "raw/"
PROCESSED_DIR = "processed/"

TRACKER_URL = 'http://webels.salt.saao.ac.za/els/#{"legend":{"tc--trajectory_status--phi":{"level":["TC","trajectory_status","Phi"],"nameId":"tc--trajectory_status--phi","nameText":"TC / trajectory status / Phi","dataType":"dbl","aggregation":"none"},"tc--trajectory_status--theta":{"level":["TC","trajectory_status","Theta"],"nameId":"tc--trajectory_status--theta","nameText":"TC / trajectory status / Theta","dataType":"dbl","aggregation":"none"},"tc--trajectory_status--rho":{"level":["TC","trajectory_status","Rho"],"nameId":"tc--trajectory_status--rho","nameText":"TC / trajectory status / Rho","dataType":"dbl","aggregation":"none"}},"settings":{"from":"<from>","to":"<to>","limit":"1000000","lines":true,"points":false}}'
POSITIONER_URL = 'http://webels.salt.saao.ac.za/els/#{"legend":{"tpc--pfgs_telemetry_information--actuator_position--0":{"level":["TPC","pfgs_telemetry_information","actuator_position","0"],"nameId":"tpc--pfgs_telemetry_information--actuator_position--0","nameText":"TPC / pfgs telemetry information / actuator position / 0","dataType":"array_dbl","aggregation":"none"},"tpc--pfgs_telemetry_information--actuator_position--2":{"level":["TPC","pfgs_telemetry_information","actuator_position","2"],"nameId":"tpc--pfgs_telemetry_information--actuator_position--2","nameText":"TPC / pfgs telemetry information / actuator position / 2","dataType":"array_dbl","aggregation":"none"},"tpc--pfgs_telemetry_information--actuator_position--3":{"level":["TPC","pfgs_telemetry_information","actuator_position","3"],"nameId":"tpc--pfgs_telemetry_information--actuator_position--3","nameText":"TPC / pfgs telemetry information / actuator position / 3","dataType":"array_dbl","aggregation":"none"},"tpc--pfgs_telemetry_information--actuator_position--5":{"level":["TPC","pfgs_telemetry_information","actuator_position","5"],"nameId":"tpc--pfgs_telemetry_information--actuator_position--5","nameText":"TPC / pfgs telemetry information / actuator position / 5","dataType":"array_dbl","aggregation":"none"}},"settings":{"from":"<from>","to":"<to>","limit":"1000000","lines":true,"points":false}}'
GUIDANCE_URL = 'http://tpc.salt/pfgs_guidance-{}.txt'



def download_dataframes(dt_from, dt_to):

    def to_ts(dt):
        return int(dt.strftime("%s"))

    def format_els_url(url, keys):
        url = url.replace('{', '(').replace('}', ')')
        url = url.replace('<', '{').replace('>', '}')
        print(url)
        url = url.format(**keys)
        url = url.replace('(', '{').replace(')', '}')

        return url

    def get_url(url):
        r = requests.get(url)

        return r.text

    time_keys = {
        'from': to_ts(dt_from),
        'to': to_ts(dt_to),
    }

    text_tracker = get_url(format_els_url(TRACKER_URL, time_keys))
    text_positioner = get_url(format_els_url(POSITIONER_URL, time_keys))
    text_guidance_f = get_url(GUIDANCE_URL.format(dt_from.strftime("%Y.%m.%d")))
    text_guidance_t = get_url(GUIDANCE_URL.format(dt_to.strftime("%Y.%m.%d")))

def save_data(filename, df):
    df.to_pickle(f'{PROCESSED_DIR}{filename}.pickle')
    # pd.read_pickle('my_df.pickle')

parser = argparse.ArgumentParser()
parser.add_argument("night", help="retrieve data for a given night [yyyymmdd]")
args = parser.parse_args()
night = datetime.strptime(args.night,"%Y%m%d")

dt_from = night + timedelta(hours=12)
dt_to = dt_from + timedelta(days=1)

df = download_dataframes(dt_from, dt_to)
