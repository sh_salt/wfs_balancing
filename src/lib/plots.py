from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix

dir_plots = ""

plot_num = 1

def init(dir_plots_in):
    global dir_plots
    dir_plots = dir_plots_in

def _save_plot(title):
    global plot_num
    filename = f"{dir_plots}{plot_num}_{title.lower().replace (' ', '_').replace ('.', '_')}"
    plt.savefig(filename)
    plt.close()
    plot_num += 1

    print("Saved:", filename)

def plot_scatter_matrix(df, title, save=True, **kwargs):
    plt.style.use('bmh')  # bmh
    scatter_matrix(df, alpha=0.2, figsize=(10, 10), **kwargs)
    plt.suptitle(title)
    _save_plot(title) if save else plt.show()

def plot_scatter(dfs, title, kwargs_scat, kwargs_ax, save=True):
    plt.style.use('bmh')  # bmh

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)

    if isinstance(dfs, pd.DataFrame):
        dfs = [dfs]
        kwargs_scat = [kwargs_scat]

    i = 0
    for df in dfs:
        try:
            ax.scatter(df.iloc[:,0],
                       df.iloc[:,1],
                       alpha=0.2,
                       c=df.iloc[:,2],
                       **kwargs_scat[i],
            )
        except TypeError:
            ax.scatter(df.iloc[:,0],
                       df.iloc[:,1],
                       c=df.iloc[:,2],
                       **kwargs_scat[i],
            )
        except:
            ax.scatter(df.iloc[:,0],
                       df.iloc[:,1],
                       alpha=0.65,
                       **kwargs_scat[i],
            )


        if 'xlim' in kwargs_ax:
            ax.set_xlim(kwargs_ax['xlim'])
        if 'ylim' in kwargs_ax:
            ax.set_ylim(kwargs_ax['ylim'])
        if 'xunit' in kwargs_ax:
            ax.xaxis.set_major_locator(plt.MultipleLocator(kwargs_ax['xunit']))
        if 'yunit' in kwargs_ax:
            ax.yaxis.set_major_locator(plt.MultipleLocator(kwargs_ax['yunit']))


        ax.set_xlabel(list(df)[0])
        ax.set_ylabel(list(df)[1])
        i += 1

    plt.title(title)
    _save_plot(title) if save else plt.show()

def plot_scatter_3d(dfs, title, kwargs, save=True):
    plt.style.use('bmh')  # bmh
    plt.rcParams['axes.facecolor'] = '0.5'

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    if isinstance(dfs, pd.DataFrame):
        dfs = [dfs]
        kwargs = [kwargs]

    i = 0
    for df in dfs:
        ax.scatter(df.iloc[:,0],
                   df.iloc[:,1],
                   df.iloc[:,2],
                   alpha=0.65,
                   c=df.iloc[:,1],
                   **kwargs[i],
        )

        ax.set_xlabel(list(df)[0])
        ax.set_ylabel(list(df)[1])
        ax.set_zlabel(list(df)[2])
        i += 1

    plt.title(title)
    _save_plot(title) if save else plt.show()
