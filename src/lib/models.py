import numpy as np

from types import SimpleNamespace as sn

class BadInputWarning(UserWarning):
    pass

_config = dict(
    A=sn(**dict(
        plate_scale = 0.2281,  # mm/arcsec
        rho_offset = 180,  # degrees
        x_offset = 0.4740,  # mm F0
        dx1 = 30.8444878,
        dx2 = -1.86E-01,
        dx3 = 6.23E-01,
    )),
    B=sn(**dict(
        plate_scale = 0.2281,  # mm/arcsec
        rho_offset = 0,  # degrees
        x_offset = 0.7545,  # mm F0
        dx1 = -30.8444878,
        dx2 = -1.86E-01,
        dx3 = 6.23E-01,
    )),
)

def get_config(AB):
    global _config
    return _config[AB[0]]

def set_config(config_in):
    global _config
    _config = config_in

def pupil_angle(AB, df):
    global _config
    rho_total = df['Rho'] + get_config(AB).rho_offset

    z1 = (-1)*np.cos(np.radians(df['Phi']))
    y1 = (-1)*np.sin(np.radians(df['Phi']))
    x1 = z1*np.sin(np.radians(df['Theta']))

    X = x1*np.cos(np.radians(rho_total)) + y1*np.sin(np.radians(rho_total))
    Y = y1*np.cos(np.radians(rho_total)) - x1*np.sin(np.radians(rho_total))
    Z = z1*np.cos(np.radians(df['Theta']))
    azimuth = np.degrees(np.arctan2(Y, X))

    pa = (180-np.degrees(np.arccos(Z/1)))*np.sin(np.radians(azimuth))
    pa *= np.cos(df['Rho'])
    # print(pa)

    return pa

def modelled_w(AB, x, pa):
    global _config
    c = get_config(AB)
    x -= c.x_offset

    x_degrees = x / 3600 / c.plate_scale
    beam_offset = (
        c.dx1 * x_degrees +
        c.dx2 * pa +
        c.dx3 * abs(x_degrees) * pa)

    return beam_offset


def _calculate_balance(left, right):
    if (left < 0).any():
        raise BadInputWarning('left input should be greater than zero', left)
    if (right < 0).any():
        raise BadInputWarning('right input should be greater than zero', right)

    return (right - left) / (right + left)

def actual_balance(AB, df):
    left = df[f'{AB} Intensity L']
    right = df[f'{AB} Intensity R']

    return _calculate_balance(left, right)

def modelled_balance(AB, df):
    # beam_width = 3  # mm
    pwfs_pupil = 12.5  # mm

    beam_radius = 2.7  # mm
    tracker_radius = 1.15  # mm

    (w, x, pa, phi, theta, rho) = (
        df['w'],
        df['x'],
        df['PA'],
        df['Phi'],
        df['Theta'],
        df['Rho'],
    )

    def area_circle(r, h):
        ''' calculate the area of a circle given the radius and area of
        interest extending from one end
        source: https://www.mathopenref.com/segmentareaht.html '''
        return r**2 * np.arccos((r - h)/r) - (r-h)*np.sqrt(2*r*h - h**2)

    beam_centre = modelled_w(AB, x, pa)  # mm from centre 0

    # # take sac pupil effects into account
    # beam_centre += pa * 0.18
    # ## beam_centre -= abs(x) * pa * 0.001
    # beam_centre += phi * +0.10
    # beam_centre += theta * +0.10

    w_steps = np.arcsin(w/3)
    ## w_steps += 0.1  # rad home error
    w = np.sin(w_steps)*3  # mm from centre 0

    # TODO: check sign!
    beam_offset = -(beam_centre - w)  # offset from centre of pwfs in mm

    r = beam_radius
    h = np.clip(r - beam_offset, 0, 2*r)  # 0 is r, r is 0
    beam_area_total = np.pi * r**2
    beam_area_left = area_circle(r, h)
    beam_area_right = beam_area_total - beam_area_left

    # TODO: check sign!
    tracker_offset = beam_offset + pa * 0.007 * np.cos(rho)

    r = tracker_radius
    h = np.clip(r - tracker_offset, 0, 2*r)  # 0 is r, r is 0
    tracker_area_total = np.pi * r**2
    tracker_area_left = area_circle(r, h)
    tracker_area_right = tracker_area_total - tracker_area_left

    beam_total = beam_area_total - tracker_area_total
    beam_left_of_w = np.maximum(0, beam_area_left - tracker_area_left)
    beam_right_of_w = np.maximum(0, beam_area_right - tracker_area_right)

    beam_left = beam_left_of_w / beam_total
    beam_right = beam_right_of_w / beam_total

    return _calculate_balance(beam_left, beam_right), (
        beam_offset, beam_radius, tracker_offset, tracker_radius)
