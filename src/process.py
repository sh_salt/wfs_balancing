import os
import math
import itertools
import numpy as np
import pandas as pd

import lib.plots as p
import lib.models as m

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

DATA_FILE = "raw/combined_data_hires.csv"
DIR_PLOTS = "plots/"

p.init(DIR_PLOTS)

for filename in os.listdir(DIR_PLOTS):
    if filename.endswith('.png'):
        os.remove(f'{DIR_PLOTS}/{filename}')

cols = pd.read_csv(DATA_FILE, nrows=1).columns
df = pd.read_csv(DATA_FILE, usecols=cols[:-1])

# print columns
# print(list(df))

# data sample
# print(df.head(5))
# print(df.dtypes)

# This looks cool, but takes a long time and means nothing...
# pd.scatter_matrix(df, alpha=0.2, figsize=(6, 6), diagonal='kde')

# print(plt.style.available)  # print list of all styles

def model_probe(AB, df):
    df = df.rename_axis('id').reset_index()
    # move id to end of colum list
    cols = df.columns.tolist()
    cols.insert(-1, cols.pop(0))
    df = df[cols]

    df['PA'] = m.pupil_angle(AB, df)
    df['x_arcmin'] = df['x'] / m.get_config(AB).plate_scale / 60
    df['w Modelled'] = m.modelled_w(AB, df['x'], df['PA'])
    df['Balance Modelled'], _ = m.modelled_balance(AB, df)
    df_wm = df
    df_wm['w'] = df_wm['w Modelled']
    df['Balance Ideal'], _ = m.modelled_balance(AB, df_wm)
    df['dx3'] = abs(df['PA']*df['x_arcmin'])

    return df


def plot_probe(AB, df):
    # print(df.head())

    def compare_modelled_vs_actual(parameter):
        p.plot_scatter(df[[parameter, 'Balance Actual', 'id']],
            f"{AB} Actual Balance vs {parameter}",{
                                    'cmap': 'nipy_spectral',
                                    'vmin': df['id'].iloc[0],
                                    'vmax': df['id'].iloc[-1],
                                    }, {'ylim':(-1.1, 1.1)})
        p.plot_scatter(df[[parameter, 'Balance Modelled', 'id']],
        f"{AB} Modelled Balance vs {parameter}",{
                                    'cmap': 'nipy_spectral',
                                    'vmin': df['id'].iloc[0],
                                    'vmax': df['id'].iloc[-1],
                                    }, {'ylim':(-1.1, 1.1)})

    df_days = [group[1] for group in df.groupby(
        (df['Date Time'] - pd.Timedelta(hours=12)).dt.date)]
    for df_day in df_days:
        p.plot_scatter(df_day[['Date Time', 'id', 'id']],
                            f"{AB} Timeframe {df_day['Date Time'].iloc[0].date()}", {
                                'alpha': 0.8,
                                'cmap': 'nipy_spectral',
                                'vmin': df['id'].iloc[0],
                                'vmax': df['id'].iloc[-1],
                            }, {
                                'xlim': (df_day['Date Time'].iloc[0],
                                         df_day['Date Time'].iloc[-1]),
                            })
    for df_day in df_days:
        p.plot_scatter(df_day[['Date Time', 'Balance Actual', 'id']],
                            f"{AB} Timeframe {df_day['Date Time'].iloc[0].date()}", {
                                'alpha': 0.8,
                                'cmap': 'nipy_spectral',
                                'vmin': df['id'].iloc[0],
                                'vmax': df['id'].iloc[-1],
                            }, {
                                'xlim': (df_day['Date Time'].iloc[0],
                                         df_day['Date Time'].iloc[-1]),
                            })

    p.plot_scatter_matrix(df[['Phi', 'Theta', 'Rho', 'PA']],
                        f"{AB} Pupil Angle")

    df_pwfs = df[abs((df['PA']*20).round()/20).isin([0, 2.25, 4.5, 6.8])]
    p.plot_scatter(df_pwfs[['x_arcmin', 'w Modelled']],
                        f"{AB} PWFS Motion vs X and PA", {}, {
                        'xlim':(-5, 5), 'ylim':(-4, 4),
                        'xunit':1, 'yunit':1})

    p.plot_scatter(df[['Balance Actual', 'Balance Modelled', 'id']],
        f"{AB} Actual Balance vs Modelled Balance",{
                                'cmap': 'nipy_spectral',
                                'vmin': df['id'].iloc[0],
                                'vmax': df['id'].iloc[-1],
                            }, {})

    p.plot_scatter(df[['Theta', 'Phi', 'id']],
    f"{AB} Phi, Theta Tracks",{
                                'cmap': 'nipy_spectral',
                                'vmin': df['id'].iloc[0],
                                'vmax': df['id'].iloc[-1],
                            }, {
                                'xlim':(7.0, -7.0),
                                'ylim':(7.0, -7.0),
                            })

    compare_modelled_vs_actual('Phi')
    compare_modelled_vs_actual('Theta')
    compare_modelled_vs_actual('Rho')
    compare_modelled_vs_actual('PA')
    compare_modelled_vs_actual('x')
    compare_modelled_vs_actual('w')
    compare_modelled_vs_actual('dx3')

    p.plot_scatter_matrix(df[['x', 'PA', 'Balance Ideal']],
                        f"{AB} Ideal Balance")
    p.plot_scatter_3d(df[['x', 'PA', 'Balance Ideal']],
                 f"{AB} Ideal Balance 3D", {'cmap':'viridis'})


def create_ideal_probe(AB):
    # Create ideal dataset
    parameter_space = {
        'Phi': np.arange(-6.5, 7.5, 1.0),
        'Theta': np.arange(-6.5, 7.5, 1.0),
        'Rho': np.arange(-90, 135, 45),
        'x': np.arange(-60, 70, 10),
        'w': np.arange(-3, 4.5, 1.5),
        }

    # Print out size of parameter_space
    total = 1
    print("Ideal Dataset")
    for key, values in parameter_space.items():
        print(key, len(values))
        total *= len(values)
    print("Total:", total)

    dataset = [dict(zip(parameter_space, vals))
               for vals in itertools.product(*parameter_space.values())]
    df = pd.DataFrame(dataset)
    # print(df)

    df = df.apply(lambda col:pd.to_numeric(col, errors='coerce'))
    df = model_probe(AB, df)
    df['Date Time'] = pd.Timestamp.now() + pd.Timedelta(seconds=1) * df['id']
    df['Balance Actual'] = df['Balance Modelled']
    # df = df[abs(df['Balance Actual']) <= 0.05]

    return df


def prepare_probe(AB, df):
    # Remove periods of HRS science
    col_SN = [f'{AB} SN']
    df = df.loc[(df[col_SN].shift() != df[col_SN]).any(axis=1)]
    df = df[df[f'{AB} SN'] >= 150]
    df = pd.concat([
        df['Date Time'].apply(lambda col: pd.to_datetime(col, errors='coerce')),
        df.iloc[:, 1:].apply(lambda col: pd.to_numeric(col, errors='coerce'))],
        axis=1)
    df['x'] = df[f'{AB}x']
    df['w'] = df[f'{AB}w']
    df['Balance Actual'] = m.actual_balance(AB, df)
    # df = df[df['Date Time'] <= (
    #     pd.Timestamp('2019-04-11') + pd.Timedelta(hours=12))]
    df = model_probe(AB, df)
    # df = df[abs(df['Balance Actual']) <= 0.20]
    # df = df[abs(df['Balance Modelled']) >= 0.10]
    # df = df[abs(df['Balance Actual'] - df['Balance Modelled']) <= 0.2]

    return df


# df_Ai = create_ideal_probe('A')
# # print(list(df_Ai))
# plot_probe('Ideal A', df_Ai)

df_A = prepare_probe('A', df)
plot_probe('A', df_A)

# df_B = prepare_probe('B', df)
# plot_probe('B', df_B)

# plt.show()
