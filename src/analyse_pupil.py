import os
import numpy as np
import pandas as pd

import lib.plots as p
import lib.models as m

DIR_PLOTS = "plots-analyse/"

p.init(DIR_PLOTS)

for filename in os.listdir(DIR_PLOTS):
    if filename.endswith('.png'):
        os.remove(f'{DIR_PLOTS}/{filename}')

def analyse_parameter(AB, df, key):
    df['PA'] = m.pupil_angle(AB, df)
    df['Balance'],(
        beam_offset, beam_radius, tracker_offset, tracker_radius
        ) = m.modelled_balance(AB, df)

    # print(beam_offset, tracker_offset)
    length = len(beam_offset)
    pf = pd.DataFrame({
        'beam_offset': beam_offset.values,
        'bracker_offset': tracker_offset.values,
        key: tracker_offset.values,
        'Balance': df['Balance'].values,
    })
    # print(df)
    # print(pf)

    area_b = np.ones(length) * np.pi * (beam_radius * 10)**2
    area_t = np.ones(length) * np.pi * (tracker_radius * 10)**2

    # plot pupil centre location and shape
    p.plot_scatter([
            pf[['beam_offset', 'Balance']],
            pf[[key, 'Balance']],
        ],
        f"{AB} {key} vs Pupil",
        [
            {'s': area_b, 'c':'r'},
            {'s': area_t, 'c':'w'},
        ],
        {})

    # plot balance and pupil shape

    # parameter offset, beam offset, tracker offset
    p_o_x_min = df[key].values.min()
    p_o_x_max = df[key].values.max()

    b_o_x_min = beam_offset.values.min()
    b_o_x_max = beam_offset.values.max()

    t_o_x_min = tracker_offset.values.min()
    t_o_x_max = tracker_offset.values.max()

    x_factor = (p_o_x_max - p_o_x_min) / (t_o_x_max - t_o_x_min)

    pf['beam_offset'] *= x_factor
    pf[key] = tracker_offset * x_factor


    p.plot_scatter([
            pf[['beam_offset', 'Balance']],
            pf[[key, 'Balance']],
        ],
        f"{AB} {key} vs Balance",
        [
            {'s': area_b, 'c':'r'},
            {'s': area_t, 'c':'w'},
        ],
        {
            # 'xlim': (p_o_x_min, p_o_x_max)
        })


def analyse_pupil(AB, parameter_space, ps_defaults):
    # for each parameter that will be populated
    for key, values in parameter_space.items():
        df = pd.DataFrame()
        df[key] = values

        # for each parameter that will be zeros
        for d_key, d_val in ps_defaults.items():
            if d_key != key:
                df[d_key] = np.ones(len(values)) * d_val

        analyse_parameter(AB, df, key)


def analyse_probe(AB):

    ps_defaults = {
        'Phi': 0,
        'Theta': 0,
        'Rho': 0,
        'x': m.get_config(AB).x_offset,
        'w': 0,
    }

    parameter_space = {
        'Phi': np.arange(-6.5, 7.5, 1.0),
        'Theta': np.arange(-6.5, 7.5, 1.0),
        'Rho': np.arange(-90, 135, 45),
        'PA': np.arange(-8.5, 9.5, 1.0),
        'x': np.arange(-60, 70, 10),
        'w': np.arange(-3, 4.0, 1.0),
    }

    for key, values in parameter_space.items():
        analyse_pupil('A Zero', parameter_space, ps_defaults)
        analyse_pupil('B Zero', parameter_space, ps_defaults)

        ps_defaults[key] = values.max()

        analyse_pupil(f'A {key}={ps_defaults[key]}', parameter_space, ps_defaults)
        analyse_pupil(f'B {key}={ps_defaults[key]}', parameter_space, ps_defaults)

analyse_probe('A')
analyse_probe('B')
